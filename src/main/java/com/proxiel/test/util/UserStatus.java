package com.proxiel.test.util;

public enum UserStatus {
	UNAUTHORIZED(1L, "UNAUTHORIZED"),
	REQUIRED_FIELD(2L, "REQUIRED FIELD"),
	OK_FOR_REGISTRATION(3L, "OK_FOR_REGISTRATION");
	
	private Long code;
	private String label;
	
	UserStatus(Long code, String label) {
		this.code = code;
		this.label = label;
	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	
}
