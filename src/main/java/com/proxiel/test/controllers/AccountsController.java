package com.proxiel.test.controllers;

import com.proxiel.test.domain.Account;
import com.proxiel.test.services.AccountService;
import com.proxiel.test.util.UserStatus;

import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/accounts/")
@Aspect
public class AccountsController {

    @Autowired
     private AccountService accountService;

     Logger logger = LoggerFactory.getLogger(AccountsController.class);

    /**
     *  Controller: Allows to register a user
     * @param account
     * @return
     */

    @PostMapping("add")
    public ResponseEntity addUser( @Valid @RequestBody Account account) {
         logger.debug("request to add User ");
         Long statusUser = accountService.addUser(account);
        if (statusUser.equals( UserStatus.OK_FOR_REGISTRATION.getCode()) ) {
            return new ResponseEntity("User Added",HttpStatus.OK);
        } else if (statusUser == UserStatus.UNAUTHORIZED.getCode()) {
            return new ResponseEntity("only adults  and that live in France can create an account!", HttpStatus.UNAUTHORIZED);
        } else if (statusUser == UserStatus.REQUIRED_FIELD.getCode()) {
            return new ResponseEntity("Required Filds!", HttpStatus.NOT_ACCEPTABLE);
        } else {
            return new ResponseEntity("Error!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     *   Controller: Allows to display the details of a registered user.
     * @param username
     * @param password
     * @return
     */
    @GetMapping("login/{username}/{password}")
    public ResponseEntity checkLogin(@PathVariable("username") String username, @PathVariable("password") String password ) {

        logger.debug("request to get logged user with username :" + username + " ; and password:" + password );
        Account account =  accountService.checkLogin(username, password);
        if ( account != null ){
            return new ResponseEntity(account, HttpStatus.OK);
        }else {
            return new ResponseEntity( "no user with provided username and password.", HttpStatus.NOT_FOUND);
        }
    }
}





