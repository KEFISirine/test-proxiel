package com.proxiel.test.services;

import com.proxiel.test.domain.Account;
import com.proxiel.test.repository.AccountRepository;
import com.proxiel.test.util.UserStatus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    LoggerService loggerService;

    /**
     *  Service : Allows to register a user
     * @param account
     * @return
     */
    public Long addUser(Account account) {
        Optional<Account> userExist = accountRepository.findByusername(account.getUsername());
        if(account.getUsername() != null && account.getEmail() != null  && account.getAge()!= 0 && account.getCountry()!= null && account.getPassword()!=null && !userExist.isPresent())  {
    	    if ( (account.getAge() > 18) && account.getCountry().equals( "France")) {
            	accountRepository.save(account);
                loggerService.logMethodeUser("User is successfully Added",HttpStatus.OK.toString());
            	return UserStatus.OK_FOR_REGISTRATION.getCode();
    	    } else{
                  loggerService.logMethodeUser("you must have more than 18 and your country must be France !",HttpStatus.UNAUTHORIZED.toString());
                     return  UserStatus.UNAUTHORIZED.getCode();
    	    }
        }else {
            System.out.println(UserStatus.REQUIRED_FIELD.getCode());
            loggerService.logMethodeUser("Required field",HttpStatus.NOT_ACCEPTABLE.toString());
            return  UserStatus.REQUIRED_FIELD.getCode();
        }
    }

    /**
     *  Service : Allows to login a user
     * @param username
     * @param password
     * @return
     */
    public Account checkLogin(String username, String password){
        Optional<Account> userExist = accountRepository.findByusernameAndpassword(username,password);
        if(userExist.isPresent()){
            loggerService.logMethodeUser("Account with username: " + username + "and password: " + password,HttpStatus.OK.toString());
        } else{
            loggerService.logMethodeUser("user not found, please create an account !",HttpStatus.NOT_FOUND.toString());
        }
        return userExist.isPresent() ? userExist.get() : null;
    }

}
