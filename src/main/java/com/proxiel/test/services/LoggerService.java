package com.proxiel.test.services;

import com.proxiel.test.domain.Logger;
import com.proxiel.test.repository.LoggerDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

@Service
@Transactional
public class LoggerService {

    @Autowired
    private LoggerDetailsRepository loggerDetailsRepository;

    /**
     * log the input and output of each call and the processing time.
     * @param message
     * @param details
     */

    public  void logMethodeUser(String message , String details  ){
        Logger log = new Logger();
        log.setMessage( message );
        log.setTimestamp(  LocalDate.now() );
        log.setDetails( details );
        loggerDetailsRepository.save(log);
    }
}
