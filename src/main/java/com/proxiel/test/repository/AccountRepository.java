package com.proxiel.test.repository;

import com.proxiel.test.domain.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {

    @Query("SELECT user from Account user where user.username = :login and user.password = :password")
    Optional<Account> findByusernameAndpassword(@Param("login") String username, @Param("password") String password);

    @Query("SELECT user from Account user where user.username = :username")
    Optional<Account> findByusername(@Param("username") String username);
}
