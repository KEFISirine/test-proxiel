package com.proxiel.test.repository;

import com.proxiel.test.domain.Logger;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoggerDetailsRepository extends JpaRepository<Logger, Long> {
}
