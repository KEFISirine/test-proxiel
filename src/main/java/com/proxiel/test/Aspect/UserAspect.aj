package com.proxiel.test.Aspect;

public aspect UserAspect {

    pointcut PC1():execution(* com.proxiel.test.controllers.AccountsController.findById(..));

    before():PC1 (){
      System.out.println("Before find details of a registered user  from  Aspectj syntax ");
    }

    after():PC1 (){
        System.out.println("After details of a registered user  from  Aspectj syntax ");
    }

    pointcut PC2():execution(* com.proxiel.test.controllers.AccountsController.addUser(..));
    before():PC2 (){
        System.out.println("Before add  from  Aspectj syntax ");
    }

    after():PC2 (){
        System.out.println("After add  from  Aspectj syntax ");
    }
}
