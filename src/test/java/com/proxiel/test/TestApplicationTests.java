package com.proxiel.test;

import java.util.Optional;

import com.proxiel.test.services.AccountService;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.proxiel.test.domain.Account;
import com.proxiel.test.repository.AccountRepository;

@SpringBootTest

class TestApplicationTests {

    @Test
    void contextLoads() {
    }
    
    @Autowired
    private AccountService accountService;
    
    @MockBean
    private AccountRepository accountRepository;
    
    @Before
    public void setUp() {
    	Optional<Account> user =  Optional.of(new Account(1L, "sirinek","sirine.kefi@esprit.tn","sirine", "kefi","France",24,"1234"));
        Mockito.when(accountRepository.findById(1L))
          .thenReturn(user);
        
        Optional<Account> user2 =  null;
        Mockito.when(accountRepository.findById(2L))
          .thenReturn(user2);
    }

}
